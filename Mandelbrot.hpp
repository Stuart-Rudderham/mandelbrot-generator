#ifndef MANDELBROT_HPP_INCLUDED
#define MANDELBROT_HPP_INCLUDED

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>

#include <limits>

// The different types of colorings you can use
enum COLOR_MODE{ RAINBOW, GREYSCALE, REDSCALE, GREENSCALE, BLUESCALE, BLACK_AND_WHITE, NUM_COLOR_MODES};

// All the functions use the "FloatingPoint" data type, which is a typedef'd built-in floating point number format
// It allows you to easily change the desired precision of the calculations
typedef long double FloatingPoint;

// This can be used to get the precision, in number of digits, of the chosen floating point format
typedef std::numeric_limits< FloatingPoint > FloatingPointPrescision;

// The dimensions of the screen
extern const int WINDOW_WIDTH;
extern const int WINDOW_HEIGHT;

/*
 * This function generates the Mandelbrot set in the region defined by the rectangle (left, top) to (left + width, top + height)
 *
 * The resolution of the picture is controled by 2 variables:
 *    maxIterations is the number of iterations done to check if the sub-block is part of the Mandelbrot set
 *
 *    subBlockSize is the size of the sub-block we are considering. If it is equal to 1 then we are calculating the color
 *    for each pixel seperately, otherwise we calculate the color for the pixel in the top-left corner of the block and apply
 *    that color to the entire sub-block.
 */
void generateMandelbrot( const FloatingPoint left, const FloatingPoint top, const FloatingPoint right, const FloatingPoint bottom, const int maxIterations, const int subBlockSize );

/*
 * This function is used to color the fractal. It uses the generated iteration values to
 * color each pixel of the fractal
 */
void colorMandelbrot( COLOR_MODE cMode, int maxIterations );

/*
 * This function is the same as the regular drawMandelbrot function, except it generates the
 * fractal multiple times, with decreasing block size each time. Obviously this takes longer
 * but it produces a nice effect as the image resolution gets higher and higher with each pass.
 * It also has the benefit of showing the user an initial, blurry image right away so that it doesn't
 * seem like the application has frozen while the higher resolution images are generated. This especially
 * important when generating very magnified images, as generating these can potentially take minutes
 */
void generateMandelbrotIterative( sf::RenderWindow& Window, COLOR_MODE cMode, FloatingPoint left, FloatingPoint top, FloatingPoint right, FloatingPoint bottom, int maxIterations );


/*
 * This function draws the generated image to the Window that is passed in. If you need to redraw the fractal
 * but *not* re-calculate it (e.g. when resizing the screen) then use this function
 */
void drawMandelbrot( sf::RenderWindow& Window );


/*
 * This function needs to be called before any of the other mandelbrot functions
 * as it creates the texture and binds it to the sprite. The width and height parameters
 * are the the dimensions of the texture the fractal is drawn onto
 */
void mandelbrotInitialize();

/*
 * This function frees any dynamically allocated memory used to create the fractal (e.g. the pixel array)
 * It's not strictly necessary to call this function as any memory allocated will be automatically reclaimed
 * by the OS when the program ends, but it's included for completeness
 */
void mandelbrotDestroy();

/*
 * This function saves the currently generated fractal as a .png image
 * It returns true if successfult, false if it was unable to save
 * The parameters are used to name the screenshot
 */
bool takeScreenshot( FloatingPoint centerX, FloatingPoint centerY );

/*
 * This function takes in an array of colors and initializes the array so the
 * the colors form some sort of gradient. This is used to make the color of points
 * that are not in the set flow together. (The Escape Time Algorithm)
 */
void setColors( COLOR_MODE cMode, int maxIterations );

#endif // MANDELBROT_HPP_INCLUDED
