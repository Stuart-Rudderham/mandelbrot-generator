#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>

#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <cassert>
#include <string>
#include <sstream>
#include <limits>

#include "Mandelbrot.hpp"

using namespace std;
using namespace sf;

// How many FPS we want to run at. This limits the event polling so that it doens't waste all your CPU time
const int TARGET_FPS = 60;

// These constants control the aspect ratio and screen dimensions
const bool squareScreen = false;

const int screenUnit = 400;

const int xScaleFactor = squareScreen ? 2 : 3;
const int yScaleFactor = squareScreen ? 2 : 2;

const int WINDOW_WIDTH = screenUnit * xScaleFactor;
const int WINDOW_HEIGHT = screenUnit * yScaleFactor;

// The name that will be displayed at the top of the window
const char* WINDOW_BASE_NAME = "Mandelbrot Renderer";

// Set this to true when you want to program to exit
bool shouldExit = false;

// This is true if the program is the active window.
bool inFocus = true;

// Forward declared functions. They are defined after the main() function
IntRect calculateRectangle( Vector2i click1, Vector2i click2 );
void calculateNewParameters( IntRect screenCoords, FloatingPoint& left, FloatingPoint& top, FloatingPoint& right, FloatingPoint& bottom );
Vector2i getMouseCoords( RenderWindow& Window );

int main() {

    // Create and open the window
    RenderWindow Window( VideoMode(WINDOW_WIDTH, WINDOW_HEIGHT, 24), WINDOW_BASE_NAME );
    Window.SetFramerateLimit(TARGET_FPS);

    // Initialize the texture the fractal will be genereated onto
    mandelbrotInitialize();

    // The parameters for drawing the fractal
    FloatingPoint left, top, right, bottom;
    int numIterations = 256;
    COLOR_MODE fractalColor = RAINBOW;

    // The initial dimensions of the fractal
    FloatingPoint initialLeft, initialTop, initialRight, initialBottom;

    // The rectangle defining our zoom in area
    IntRect magnifyArea( 0, 0, WINDOW_WIDTH, WINDOW_HEIGHT );

    // Set up the initial dimensions
    if( squareScreen ) {
        initialLeft = -2;
        initialTop = 2;

        initialRight = 2;
        initialBottom = -2;
    } else {
        initialLeft = -2;
        initialTop = 1;

        initialRight = 1;
        initialBottom = -1;
    }

    left = initialLeft;
    top = initialTop;
    right = initialRight;
    bottom = initialBottom;

    // Draw the initial picture
    generateMandelbrotIterative(Window, fractalColor, left, top, right, bottom, numIterations);

    // Used to handle window events (e.g. clicking the close button)
    Event Event;

    // Used to calculate the region to zoom into
    Vector2i mouseClickLocation, mouseReleaseLocation;

    // Used to deal with user input
    bool mouseClicked = false;
    bool colorChangeKeyDown = false;

    // Set the printing precision for floating point numbers
    cout.precision( FloatingPointPrescision::digits10 );

    while( !shouldExit ) {

        // Clear out the event queue
        while( Window.PollEvent(Event) ) {

            // If the user tries to close the window
            if( Event.Type == Event::Closed) {

                // Exit the program
                shouldExit = true;

            // If the user tries to resize the window
            } else if( Event.Type == Event::Resized ) {
                cout << "The window has been resized.\n";
                cout << "The original resolution of (" << WINDOW_WIDTH << "x" << WINDOW_HEIGHT << ") is being scaled to (" << Event.Size.Width << "x" << Event.Size.Height << ")\n\n";

                // Redraw (not re-generate) the mandelbrot for the resized window
                drawMandelbrot( Window );

                // Update the screen
                Window.Display();

            } else if( Event.Type == Event.LostFocus ) {
                cout << "Window not in focus anymore\n\n";

                inFocus = false;

                mouseClicked = false;

            } else if( Event.Type == Event.GainedFocus ) {
                cout << "Window back in focus\n\n";

                inFocus = true;

                mouseClicked = false;
            }
        }


        // Make sure the window is in focus before doing anything
        if( inFocus ) {

            // Make sure the user isn't trying to increase and decrease the resolution at the same time
            if( !(Keyboard::IsKeyPressed(Keyboard::PageUp) && Keyboard::IsKeyPressed(Keyboard::PageDown)) ) {
                if( Keyboard::IsKeyPressed(Keyboard::PageUp) ) {
                    cout << "Increased the number of iterations from " << numIterations;

                    numIterations *= 2;

                    cout << " to " << numIterations << "\n\n";

                    generateMandelbrotIterative(Window, fractalColor, left, top, right, bottom, numIterations);
                }

                if( Keyboard::IsKeyPressed(Keyboard::PageDown) ) {
                    assert( numIterations >= 1 );

                    int oldMax = numIterations;

                    numIterations /= 2;

                    if( numIterations < 1 ) {
                        numIterations = 1;
                    }

                    if( numIterations != oldMax ) {
                        cout << "Decreased the number of iterations from " << oldMax << " to " << numIterations << "\n\n";
                        generateMandelbrotIterative(Window, fractalColor, left, top, right, bottom, numIterations);
                    } else {
                        cout << "Could not decrease the number of iterations below " << numIterations << "\n\n";
                    }
                }
            }

            // Take a screenshot
            if( Keyboard::IsKeyPressed(Keyboard::S) ) {
                if( takeScreenshot( (left + right) / 2.0f, (top + bottom) / 2.0f ) ) {
                    cout << "Screen shot taken\n\n";
                } else {
                    cout << "Error occured, screen shot not saved\n\n";
                }
            }

            // Quit the program is the Q key is pressed
            if( Keyboard::IsKeyPressed(Keyboard::Q) ) {
                shouldExit = true;

                cout << "Quitting ...\n";
            }

            // Changing the color scheme
            if( Keyboard::IsKeyPressed(Keyboard::Num2) ) {

                if( colorChangeKeyDown == false ) {
                    cout << "Changing color scheme to next color\n\n";

                    // Choose the next color in the sequence
                    int nextColor = (int)fractalColor + 1;

                    if( nextColor == (int)NUM_COLOR_MODES ) {
                        nextColor = 0;
                    }

                    fractalColor = (COLOR_MODE)nextColor;

                    // Update the color scheme, draw it, and update the window
                    colorMandelbrot( fractalColor, numIterations );
                    drawMandelbrot( Window );
                    Window.Display();
                }

                colorChangeKeyDown = true;

            } else if( Keyboard::IsKeyPressed(Keyboard::Num1) ) {

                if( colorChangeKeyDown == false ) {
                    cout << "Changing color scheme to previous color\n\n";

                    // Choose the previous color in the sequence
                    int nextColor = (int)fractalColor - 1;

                    if( nextColor == -1 ) {
                        nextColor = (int)NUM_COLOR_MODES - 1;
                    }

                    fractalColor = (COLOR_MODE)nextColor;

                    // Update the color scheme, draw it, and update the window
                    colorMandelbrot( fractalColor, numIterations );
                    drawMandelbrot( Window );
                    Window.Display();
                }

                colorChangeKeyDown = true;

            } else {
                colorChangeKeyDown = false;
            }


            // If the left mouse button is down
            if( Mouse::IsButtonPressed(Mouse::Left) ) {

                // and this is the first time you're clicking it
                if( mouseClicked == false ) {

                    // Store the location you clicked
                    mouseClickLocation = getMouseCoords( Window );
                    mouseClicked = true;
                }

            // If the left mouse button is not down and mouseClicked is true then you must have just released the mouse button
            } else if( mouseClicked ) {

                // store the location where you release
                mouseReleaseLocation = getMouseCoords( Window );

                // Make sure the user actually moved the mouse
                if( mouseReleaseLocation != mouseClickLocation ) {

                    // Given the two locations click and the rectangle they define, calculate the
                    // new coordinates to draw the fractal at. We keep track of the old values
                    // because if we can't zoom in anymore (due to floating point precision) then
                    // we revet back to the old values and display a message
                    FloatingPoint oldLeft = left;
                    FloatingPoint oldRight = right;
                    FloatingPoint oldTop = top;
                    FloatingPoint oldBottom = bottom;

                    magnifyArea = calculateRectangle( mouseClickLocation, mouseReleaseLocation );
                    calculateNewParameters( magnifyArea, left, top, right, bottom );

                    // If the parameters are not valid (also accounts for subnormal numbers)
                    if( (right - left) <= FloatingPointPrescision::epsilon() || (top - bottom) <= FloatingPointPrescision::epsilon() ) {
                        cout << "Cannot zoom in any further\n";
                        cout << "\tleft - " << left << endl;
                        cout << "\tright - " << right << endl;
                        cout << "\ttop - " << top << endl;
                        cout << "\tbottom - " << bottom << endl;
                        cout << "\twidth - " << right - left << endl;
                        cout << "\theight - " << top - bottom << endl;
                        cout << "\n";

                        // If not then revert back
                        left = oldLeft;
                        right = oldRight;
                        top = oldTop;
                        bottom = oldBottom;

                    // If the parameters are valid
                    } else {
                        cout << "width - " << right - left << endl;
                        cout << "height - " << top - bottom << endl;

                        // generate the fractal at these coordinates
                        generateMandelbrotIterative(Window, fractalColor, left, top, right, bottom, numIterations);
                    }
                }

                mouseClicked = false;
            }

            // If you're holding down the mouse button then draw the zoom in rectangle
            if( mouseClicked ) {

                // get the current location of the mouse
                mouseReleaseLocation = getMouseCoords( Window );

                // Generate the rectangle outlining the area of the screen you want to magnify
                magnifyArea = calculateRectangle( mouseClickLocation, mouseReleaseLocation );

                // Draw the fractal
                drawMandelbrot( Window );

                // Draw the ractangle
                Window.Draw( Shape::Rectangle( magnifyArea.Left, magnifyArea.Top, magnifyArea.Width, magnifyArea.Height, Color(255, 255, 255, 128), 2.0f, Color::White ) );

                // Update the screen
                Window.Display();
            }
        }
    }

    // Free any allocated memory
    mandelbrotDestroy();

    Window.Close();
    return 0;
}

/*
 * This function gets the current mouse coordinates and adjusts them for the window size
 * If the window has been adjusted then the screen coordinates will not match the mouse
 * coordinates, so we adjust them
 */
Vector2i getMouseCoords( RenderWindow& Window ) {

    // store the location of the mouse
    Vector2i mouseCoords = Mouse::GetPosition( Window );

    // If the window has been resized then the mouse coordinates and view coordinates wil not match, so we need to convert them
    Vector2f adjustedPosition = Window.ConvertCoords( mouseCoords.x, mouseCoords.y );

    // Return the adjusted coordinates
    mouseCoords.x = (int)adjustedPosition.x;
    mouseCoords.y = (int)adjustedPosition.y;

    return mouseCoords;
}


/*
 * This function takes in two points and calculates the rectangle formed by them.
 * The rectangle is anchored by the first point, everything is calculated relative to it.
 *
 * The function is used to calculate the new area
 */
IntRect calculateRectangle( Vector2i click1, Vector2i click2 ) {

    IntRect returnValue;

    // If the first click is the top-left corner of the rectangle
    if( click1.x <= click2.x && click1.y <= click2.y ) {
        returnValue.Left = click1.x;
        returnValue.Top = click1.y;

        returnValue.Width = click2.x - click1.x;
        returnValue.Height = int( (float)returnValue.Width * (float)yScaleFactor / (float)xScaleFactor );

    // if the first click is the bottom-right corner of the rectangle
    } else {
        returnValue.Width = click1.x - click2.x;
        returnValue.Height = int( (float)returnValue.Width * (float)yScaleFactor / (float)xScaleFactor );

        returnValue.Left = click2.x;
        returnValue.Top = click1.y - returnValue.Height;
    }

    return returnValue;
}

/*
 * This function takes in the rectangle that defines the area on the screen you
 * want to magnify and the current fractal coordinates and modifies the the current
 * fractal coordinates to reflect the area we are magnifying
 */
void calculateNewParameters( IntRect screenCoords, FloatingPoint& left, FloatingPoint& top, FloatingPoint& right, FloatingPoint& bottom ) {

    // Convert the coordinates of the zoom in rectangle into the local Mandelbrot coordinates
    FloatingPoint xFactor = (FloatingPoint)screenCoords.Left / (FloatingPoint)WINDOW_WIDTH;
    FloatingPoint yFactor = (FloatingPoint)screenCoords.Top / (FloatingPoint)WINDOW_HEIGHT;

    FloatingPoint widthFactor = (FloatingPoint)screenCoords.Width / (FloatingPoint)WINDOW_WIDTH;
    FloatingPoint heightFactor = (FloatingPoint)screenCoords.Height / (FloatingPoint)WINDOW_HEIGHT;

    // Calculate the ractangle in fractal coordinates
    FloatingPoint width = right - left;
    FloatingPoint height = bottom - top;

    left += width * xFactor;
    top += height * yFactor;

    right = left + width * widthFactor;
    bottom = top + height * heightFactor;
}
