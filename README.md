# Controls #

| Action                          | Key            |
|---------------------------------|----------------|
| Zoom                            | Click and drag |
| Increase iterations             | PageUp         |
| Decrease iterations             | PageDown       |
| Take a screenshot               | S              |
| Quit                            | Q              |
| switch to previous color scheme | 1              |
| switch to next color scheme     | 2              |

# Screenshot #

![](./screenshot.png "Screenshot")
