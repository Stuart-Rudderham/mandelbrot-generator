#include <iostream>
#include <cassert>
#include <sstream>

#include "Mandelbrot.hpp"

using namespace std;
using namespace sf;

// Where the picture will be drawn to
static Texture mandelbrotTexture;

// Needed to draw the texture to the screen
static Sprite mandelbrotSprite;

// Since the Texture lives on the graphics card memory, need this array to do the pixel color update all at once
static Uint8* pixels;

// This array hold the number of iterations for each pixel. It is a seperate array so that if the color scheme
// changes we don't have to recalculate all the values
static unsigned int* fractalIterations;

// The color table
static Color* colors;

// If the iteration value exceeds this value we consider it to not be a part of the set
static const int bailoutValueSquared = 4;

// Used to make sure that the mandelbrotInitialize() function is called before anything else
static bool mandelbrotInitialized = false;



/*
 * This function generates the Mandelbrot set in the region defined by the rectangle (left, top) to (left + width, top + height)
 */
void generateMandelbrot( Color* colors, const FloatingPoint left, const FloatingPoint top, const FloatingPoint right, const FloatingPoint bottom, const int maxIterations, const int subBlockSize = 1 ) {
    assert( mandelbrotInitialized );

    // The width and height of the rectangle defining the region we want to draw
    const FloatingPoint width = right - left;
    const FloatingPoint height = top - bottom;

    // Make sure the rectangle is defined properly
    assert( right > left );
    assert( top > bottom );
    assert( width > 0 );
    assert( height > 0 );

    // The values that C increases by each pixel
    const FloatingPoint rowShiftAmount = height / (FloatingPoint)WINDOW_HEIGHT;
    const FloatingPoint colShiftAmount = width / (FloatingPoint)WINDOW_WIDTH;

    // For every pixel on the screen, decide if it's part of the set
    FloatingPoint rowOffset = 0;
    for( int row = 0; row < WINDOW_HEIGHT; row += subBlockSize ) {

        FloatingPoint colOffset = 0;

        for( int col = 0; col < WINDOW_WIDTH; col += subBlockSize ) {

            // The constant complex number C
            FloatingPoint cR = left + colOffset;         // the FloatingPoint part
            FloatingPoint cI = top - rowOffset;          // the imaginary part

            // The complex number Z
            FloatingPoint zR = 0;                      // FloatingPoint part
            FloatingPoint zI = 0;                      // imaginary part

            // Iterate through the sequence, checking to see if the pixel is part of the set
            int i;
            for( i = 0; i < maxIterations; i++ ) {
                FloatingPoint tempZR = zR;             // hold the FloatingPoint part of the complex number since
                                                // squaring the imaginary part requires the FloatingPoint part

                // Square the complex number Z
                zR = (zR * zR) - (zI * zI);
                zI = 2 * tempZR * zI;

                // Add the constant complex number C
                zR += cR;
                zI += cI;

                // Check if the sequence is diverging
                if( (zR * zR) + (zI * zI) > bailoutValueSquared ) {
                    break;
                }
            }

            // Now we set the number of iterations it took to escape
            int minY = min( WINDOW_HEIGHT, row + subBlockSize );            // make sure we don't try and write off the end of the array
            int minX = min( WINDOW_WIDTH, col + subBlockSize );

            // Set the iterations to be the same for the entire sub-block
            for( int y = row; y < minY; y++ ) {
                for( int x = col; x < minX; x++ ) {
                    fractalIterations[y * WINDOW_WIDTH + x] = i;
                }
            }

            // Increase the x offset for the complex number C
            colOffset += colShiftAmount * subBlockSize;
        }

        // Increase the y offset for the complex number C
        rowOffset += rowShiftAmount * subBlockSize;


        // Update the progress bar
        cout << "\tSub-block size of " << subBlockSize << " - " << int(100.0f * (float)row / float(WINDOW_HEIGHT)) << "%\r";
    }

    cout << "\tSub-block size of " << subBlockSize << " - 100%\n";
}


/*
 * Generate multiple Mandelbrot fractals at increasing resolutions
 */
void generateMandelbrotIterative( RenderWindow& Window, COLOR_MODE cMode, FloatingPoint left, FloatingPoint top, FloatingPoint right, FloatingPoint bottom, int maxIterations ) {
    assert( mandelbrotInitialized );

    // Make sure the rectangle is defined properly
    assert( right > left );
    assert( top > bottom );

    // Set up the colors, since they don't depend on the block size
    delete [] colors;
    colors = new Color[maxIterations + 1];
    setColors( cMode, maxIterations );

    // The starting sub-block size
    const int startingBlockSize = 256;

    // Make sure the starting sub-block size is positive and a power of 2
    assert( startingBlockSize > 0 && ( startingBlockSize & (startingBlockSize - 1) ) == 0 );

    cout << "Drawing Mandelbrot Set in rectangle (" << left << ", " << top << "), (" << right << ", " << bottom << ") with at most " << maxIterations << " iterations per block.\n";

    // The clock used to time how long it takes to render
    Clock drawTime;

    // Set the clock to 0
    drawTime.Reset();

    // Draw the Mandelbrot set with increasing smaller block size
    for( int i = startingBlockSize; i >= 1; i /= 4 ) {

        // Generate the fractal
        generateMandelbrot( colors, left, top, right, bottom, maxIterations, i);

        // Color it in
        colorMandelbrot( cMode, maxIterations );

        // Draw it to the screen
        drawMandelbrot( Window );

        // Update the screen
        Window.Display();

        //Sleep(100);
    }

    cout << "Done! Total drawing time was " << drawTime.GetElapsedTime() << " milliseconds\n\n";
}

/*
 * Used to color the fractal
 */
void colorMandelbrot( COLOR_MODE cMode, int maxIterations ) {

    setColors( cMode, maxIterations );

    // For each pixel on the screen
    for( int row = 0; row < WINDOW_HEIGHT; row++ ) {
        for( int col = 0; col < WINDOW_WIDTH; col++ ) {

            int numIterations = fractalIterations[row * WINDOW_WIDTH + col];

            // Set the color based on the number of iterations
            pixels[ (row * WINDOW_WIDTH + col) * 4 + 0 ] = colors[numIterations].r;         // Red
            pixels[ (row * WINDOW_WIDTH + col) * 4 + 1 ] = colors[numIterations].g;         // Green
            pixels[ (row * WINDOW_WIDTH + col) * 4 + 2 ] = colors[numIterations].b;         // Blue
            pixels[ (row * WINDOW_WIDTH + col) * 4 + 3 ] = 255;                             // Alpha
        }
    }

    // Update the texture with the new pixel colors
    mandelbrotTexture.Update( pixels );
}

/*
 * Used to draw the generated fractal onto the screen
 */
void drawMandelbrot( RenderWindow& Window ) {
    assert( mandelbrotInitialized );

    Window.Draw( mandelbrotSprite );
}


/*
 * Needs to be called before anything else to initialize the texture/sprite
 */
void mandelbrotInitialize() {

    // Create the texture
    mandelbrotTexture.Create( WINDOW_WIDTH, WINDOW_HEIGHT );

    // Turn on simple anti-aliasing when drawing the texture.
    // This is not the same as anti-aliasing when generating the fractal, which has not been implemented as of Nov 30, 2011
    // Not sure if this will actually do anything, additional testing is needed
    mandelbrotTexture.SetSmooth( true );

    // Set the texture to the sprite so it can be drawn
    mandelbrotSprite.SetTexture( mandelbrotTexture );

    // Create the pixel array that gets written to when generating the fractal
    pixels = new Uint8[WINDOW_WIDTH * WINDOW_HEIGHT * 4];

    // Create the iteration value array
    fractalIterations = new unsigned int[WINDOW_WIDTH * WINDOW_HEIGHT];

    mandelbrotInitialized = true;
}


/*
 * Free any allocated memory
 */
void mandelbrotDestroy() {
    delete [] pixels;
    delete [] fractalIterations;
    delete [] colors;

    mandelbrotInitialized = false;
}


/*
 * Take a screenshot
 */
bool takeScreenshot( FloatingPoint centerX, FloatingPoint centerY ) {

    // Get the filename
    stringstream filename;
    filename << "Mandelbrot Set centered at (" << centerX << ", " << centerY << ").png";

    // Copy the texture (which lives on the graphic card's memory) to an image (which lives in main memory)
    Image screenshot = mandelbrotTexture.CopyToImage();

    // Save the screenshot, returning whether it succeeded or not
    return screenshot.SaveToFile( filename.str() );
}


/*
 * Used to initialize the array of colors
 */
void setColors( COLOR_MODE cMode, int maxIterations ) {

    // Make sure the color mode passed in is valid
    assert( cMode < NUM_COLOR_MODES );

    // Rainbow Gradient
    // Algorithm values chosen through experimentation
    if( cMode == RAINBOW ) {
        const int boundryIteration = maxIterations / 6;

        for( int c = 0; c < maxIterations; c++ ) {
            if( c < boundryIteration ) {
                colors[c] = Color( 255, (c % boundryIteration) * 6, 0 );

            } else if( c < boundryIteration * 2 ) {
                colors[c] = Color( 255 - (c % boundryIteration) * 6, 255, 0 );

            } else if( c < boundryIteration * 3 ) {
                colors[c] = Color( 0, 255, (c - boundryIteration * 2) * 6 );

            } else if( c < boundryIteration * 4 ) {
                colors[c] = Color( 0, 255 - (c - boundryIteration * 2) * 6, 255 );

            } else if( c < boundryIteration * 5 ) {
                colors[c] = Color( (c - boundryIteration * 2) * 6, 0, 255 );

            } else {
                colors[c] = Color( 255, 0, 255 - (c - boundryIteration * 2) * 6 );
            }
        }
        colors[maxIterations] = Color::Black;

    } else if( cMode == GREYSCALE ) {
        for( int c = 0; c < maxIterations; c++ ) {
            int shade = 255 * (float)c / (float)maxIterations;
            colors[c] = Color( shade, shade, shade, 255 );
        }
        colors[maxIterations] = Color::Black;

    } else if( cMode == REDSCALE ) {
        for( int c = 0; c < maxIterations; c++ ) {
            int shade = 255 * (float)c / (float)maxIterations;
            colors[c] = Color( shade, 0, 0, 255 );
        }
        colors[maxIterations] = Color::Black;

    } else if( cMode == GREENSCALE ) {
        for( int c = 0; c < maxIterations; c++ ) {
            int shade = 255 * (float)c / (float)maxIterations;
            colors[c] = Color( 0, shade, 0, 255 );
        }
        colors[maxIterations] = Color::Black;

    } else if( cMode == BLUESCALE ) {
        for( int c = 0; c < maxIterations; c++ ) {
            int shade = 255 * (float)c / (float)maxIterations;
            colors[c] = Color( 0, 0, shade, 255 );
        }
        colors[maxIterations] = Color::Black;

    } else if( cMode == BLACK_AND_WHITE ) {
        for( int c = 0; c < maxIterations; c++ ) {
            colors[c] = Color::Black;
        }
        colors[maxIterations] = Color::White;
    }

}
